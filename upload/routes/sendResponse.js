/**
 * Created by ABC on 2/27/2015.
 */
exports.somethingWentWrongError = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.ERROR_IN_EXECUTION,
        data: {}
    }
    sendData(errResponse,res);
};

exports.fileUploadError = function (res,message) {
    var errResponse = {
        status: constant.responseStatus.FILE_UPLOAD,
        message: "",
        data: message
    }
    sendData(errResponse,res);
};

exports.sendSuccessData = function (res,data) {

    var successResponse = {
        status: constant.responseStatus.SHOW_DATA,
        message: "",
        data: data
    };
    sendData(successResponse,res);
};
exports.parameterMissingError = function (res) {

    var errResponse = {
        status: constant.responseStatus.PARAMETER_MISSING,
        message: constant.responseMessage.PARAMETER_MISSING,
        data: {}
    }
    sendData(errResponse,res);
};
exports.sendData = function (data,res) {
    sendData(data,res);
};


function sendData(data,res)
{
    res.type('json');
    res.jsonp(data);
}