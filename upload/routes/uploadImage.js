var express = require('express');
var router = express.Router();
var sendResponse=require('./sendResponse');
var func=require('./commonfunction');
/* GET users listing. */
router.post('/upload_image', function(req, res, next) {

    var newRandomName=func.generateRandomNumber();
    var fileName=[req.files.file.name];

   // check the miising any value
  var manValues=[fileName];

    func.checkBlank(res,manValues,function(result){
        //does not missing any value
        if(result==1){
        func.uploadImage(req.files.file,newRandomName,function(result_image){
            if(result_image===0){
                console.log("does upload image generate the error",result_image);
                 sendResponse.fileUploadError(res,"upload image generate the error");
            }
            else{
                   sendResponse.sendSuccessData(res,result_image);
            }
        });
        }
    });
});

module.exports = router;
