/**
 * Created by ABC on 2/27/2015.
 */
var sendResponse=require('./sendResponse');
var math=require('math');
var fs=require('fs');
exports.checkBlank = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);
    console.log(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(1);
    }
}

function checkBlank(arr) {

    var arrlength = arr.length;

    for (var i = 0; i < arrlength; i++) {
        if (arr[i] == '') {
            return 1;
            break;
        }
        else if (arr[i] == undefined) {
            return 1;
            break;
        }
        else if (arr[i] == '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}
exports.generateRandomNumber =function() {

    var timestamp = new Date().getTime().toString();
    var str = '';
    var chars = "abcdefghijkulmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
    var size = chars.length;

    for (var i = 0; i < 4; i++) {
        var randno = math.floor(math.random() * size);
        str = chars[randno] + str;
    }
    return (str+timestamp);
}

exports.uploadImage=function(file,newName,callback1){
    var fileName=file.name;
    var path=file.path;
    var ext=fileName.split('.').pop();
    fs.readFile(path,function(err,file_buffer){
        //file read into the filesystem
        if(err){
            console.log("file does not read");
            return callback1(0);
        }
        else{
            //set the file path new
            var newPath = "./public/images/"+newName+"."+ext;
            fs.writeFile(newPath,file_buffer, function (err,res) {
                console.log(newPath);
                if(err){
                    console.log("file does not write",err);
                    callback1(0);
                }
                else {
                    return callback1(newPath);
                }
            });

        }
    });
}